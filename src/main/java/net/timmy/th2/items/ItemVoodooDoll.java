package net.timmy.th2.items;

import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.timmy.th2.Thaumania2;

public class ItemVoodooDoll extends Item {

    public ItemVoodooDoll() {
        super("voodoo_doll");
        setCreativeTab(Thaumania2.th2Items);
    }

}
