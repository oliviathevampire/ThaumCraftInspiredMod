package net.timmy.th2.items;

import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.timmy.th2.Thaumania2;

public class ItemChalk extends Item {

    public ItemChalk() {
        super("chalk");
        setCreativeTab(Thaumania2.th2Items);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
    }

}
