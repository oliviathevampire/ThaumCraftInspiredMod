package net.timmy.th2.items;

import net.thegaminghuskymc.huskylib2.lib.items.ItemMod;
import net.timmy.th2.Reference;

public class ItemWandCap extends ItemMod {

    public ItemWandCap(String name) {
        super("wand_cap_" + name);
    }

    @Override
    public String getModNamespace() {
        return Reference.MODID;
    }

}
