package net.timmy.th2.lib;

import net.minecraft.util.ResourceLocation;

public final class LibMisc {

    // Mod Constants
    public static final String MOD_ID = "th2";
    public static final String MOD_NAME = MOD_ID;
    public static final String BUILD = "GRADLE:BUILD";
    public static final String VERSION = "GRADLE:VERSION-" + BUILD;

    // Proxy Constants
    public static final String PROXY_COMMON = "net.timmy.th2.proxy.CommonProxy";
    public static final String PROXY_CLIENT = "net.timmy.th2.proxy.ClientProxy";

    public static final String MOD_WEBSITE = "http://thegaminghuskymc.cba.pl/";

    public static final ResourceLocation GENERAL_ICONS_RESOURCE = new ResourceLocation(MOD_ID, "textures/misc/general_icons.png");

}
