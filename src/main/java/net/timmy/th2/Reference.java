package net.timmy.th2;

import net.timmy.th2.lib.LibMisc;

public class Reference {

    public static final String MODID = LibMisc.MOD_ID;
    public static final String NAME = LibMisc.MOD_NAME;
    public static final String VERSION = LibMisc.VERSION;
    public static final String CSIDE = LibMisc.PROXY_CLIENT;
    public static final String SSIDE = LibMisc.PROXY_COMMON;

}
