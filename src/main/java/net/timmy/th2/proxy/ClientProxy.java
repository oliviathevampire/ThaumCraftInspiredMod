package net.timmy.th2.proxy;

import net.minecraft.block.BlockColored;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.thegaminghuskymc.huskylib2.lib.utils.ModelHandler;
import net.timmy.th2.Reference;
import net.timmy.th2.init.Th2Blocks;
import net.timmy.th2.init.Th2Entities;
import net.timmy.th2.init.Th2Items;
import net.timmy.th2.items.Item;

public class ClientProxy extends CommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        super.preInit(event);
        MinecraftForge.EVENT_BUS.register(ModelHandler.class);
        Th2Entities.register();
        
        // Skipping 0 as it already works
        ModelResourceLocation model = new ModelResourceLocation(new ResourceLocation(Reference.MODID, "chalk"), "inventory");
        for(int i = 1; i < 16; i++) {
        	ModelLoader.setCustomModelResourceLocation(Th2Items.chalk, i, model);
        }
    }

    @Override
    public void init(FMLInitializationEvent event) {
        super.init(event);
        Th2Entities.registerRenders();

        /*ItemColors itemColors = Minecraft.getMinecraft().getItemColors();
        BlockColors blockColors = Minecraft.getMinecraft().getBlockColors();

        for (IBlockColored b : Th2Blocks.pendingIBlockColorBlocks) {
            blockColors.registerBlockColorHandler(b::getColorMultiplier, (Block) b);
        }

        for (IItemColored i : Th2Items.pendingDynamicColorItems) {
            itemColors.registerItemColorHandler(i::colorMultiplier, (Item) i);
        }*/

        ItemColors ic = Minecraft.getMinecraft().getItemColors();
		BlockColors bc = Minecraft.getMinecraft().getBlockColors();
		
		bc.registerBlockColorHandler((state, world, pos, index) -> {
			try {
				EnumDyeColor color = state.getValue(BlockColored.COLOR);
				return color.getColorValue();
			} catch(Exception e) {
				return 0xFFFFFF;
			}
		}, Th2Blocks.candles);
		
		ic.registerItemColorHandler((item, index) -> {
			try {
				EnumDyeColor color = EnumDyeColor.byMetadata(item.getItemDamage());
				return color.getColorValue();
			} catch(Exception e) {
				return 0xFFFFFF;
			}
		}, Th2Items.chalk, Th2Items.voodoo_doll, Item.getItemFromBlock(Th2Blocks.candles));
    }

    @Override
    public void postInit(FMLPostInitializationEvent event) {
        super.postInit(event);
    }

}
