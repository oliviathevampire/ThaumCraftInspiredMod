package net.timmy.th2.init;

import net.minecraft.item.EnumDyeColor;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.timmy.th2.Thaumania2;
import net.timmy.th2.items.*;
import net.timmy.th2.properties.*;

import java.util.LinkedList;
import java.util.List;

public class Th2Items {

    public static List<IItemColored> pendingDynamicColorItems = new LinkedList<>();

    private static net.minecraft.item.Item wandCore, wandCap, wand, rune, scroll;

    public static net.minecraft.item.Item voodoo_doll, chalk;

    static {

        for (EnumWandCoreType type : EnumWandCoreType.values()) {
            wandCore = new ItemWandCore(type.getName()).setCreativeTab(Thaumania2.th2Wands);
        }

        for (EnumWandCapType type : EnumWandCapType.values()) {
            wandCap = new ItemWandCap(type.getName()).setCreativeTab(Thaumania2.th2Wands);
        }

        for (EnumWandCombinationsDefault type : EnumWandCombinationsDefault.values()) {
            wand = new ItemWand(type.getName()).setCreativeTab(Thaumania2.th2Wands);
        }

        chalk = new ItemChalk();
        register(chalk);
        voodoo_doll = new ItemVoodooDoll();
        register(voodoo_doll);

        for (EnumRuneType type : EnumRuneType.values()) {
            rune = new Item("rune_" + type.getName()).setCreativeTab(Thaumania2.th2Items);
            scroll = new Item("scroll_" + type.getName()).setCreativeTab(Thaumania2.th2Items);
        }

        for (EnumRuneType2 type : EnumRuneType2.values()) {
            rune = new Item("rune_" + type.getName()).setCreativeTab(Thaumania2.th2Items);
            scroll = new Item("scroll_" + type.getName()).setCreativeTab(Thaumania2.th2Items);
        }

    }

    private static <T extends IForgeRegistryEntry<T>> void register(T item) {

        if (item instanceof Item) {
            if(item instanceof IItemColored) {
                pendingDynamicColorItems.add((IItemColored) item);
            }
        }
    }

    public static void register() {

    }

}
