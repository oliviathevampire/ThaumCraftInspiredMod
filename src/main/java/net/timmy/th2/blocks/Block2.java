package net.timmy.th2.blocks;

import net.thegaminghuskymc.huskylib2.lib.blocks.BlockBase;
import net.timmy.th2.Reference;
import net.timmy.th2.Thaumania2;

public class Block2 extends BlockBase {

    public Block2(String name) {
        super(Reference.MODID, name, Thaumania2.th2Blocks);
    }

}