package net.timmy.th2.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.timmy.th2.blocks.base.ModBlock;

public class Block extends ModBlock {

    public Block(Material material, String name) {
        super(material, name);
    }

    @Override
    public IProperty getVariantProp() {
        return null;
    }

    @Override
    public IProperty[] getIgnoredProperties() {
        return new IProperty[0];
    }

    @Override
    public EnumRarity getBlockRarity(ItemStack itemStack) {
        return EnumRarity.COMMON;
    }

    @Override
    public Class getVariantEnum() {
        return null;
    }

}