package net.timmy.th2.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.thegaminghuskymc.huskylib2.lib.utils.ItemUtils;
import net.timmy.th2.blocks.base.ModBlock;

import javax.annotation.Nullable;
import java.util.List;

public class Rune2 extends ModBlock {

    public Rune2(String name) {
        super(Material.ROCK, name);
    }

    @Override
    public IProperty getVariantProp() {
        return null;
    }

    @Override
    public IProperty[] getIgnoredProperties() {
        return new IProperty[0];
    }

    @Override
    public EnumRarity getBlockRarity(ItemStack itemStack) {
        return EnumRarity.COMMON;
    }

    @Override
    public Class getVariantEnum() {
        return null;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World player, List<String> tooltip, ITooltipFlag advanced) {
        ItemUtils.addInformationRunes2(stack, tooltip);
    }

}