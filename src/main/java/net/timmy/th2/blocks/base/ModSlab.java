package net.timmy.th2.blocks.base;

import net.minecraft.block.material.Material;
import net.thegaminghuskymc.huskylib2.lib.blocks.BlockModSlab;
import net.thegaminghuskymc.huskylib2.lib.interf.IModBlock;
import net.timmy.th2.Reference;

public abstract class ModSlab extends BlockModSlab implements IModBlock {

    public ModSlab(String name, Material materialIn, boolean doubleSlab) {
        super(name, materialIn, doubleSlab);
    }

    @Override
    public String getModNamespace() {
        return Reference.MODID;
    }

}
