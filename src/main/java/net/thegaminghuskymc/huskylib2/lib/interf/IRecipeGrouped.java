package net.thegaminghuskymc.huskylib2.lib.interf;

public interface IRecipeGrouped {

    public String getRecipeGroup();

}
