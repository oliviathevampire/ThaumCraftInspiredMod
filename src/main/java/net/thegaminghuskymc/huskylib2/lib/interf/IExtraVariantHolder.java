package net.thegaminghuskymc.huskylib2.lib.interf;

public interface IExtraVariantHolder extends IVariantHolder {

    public String[] getExtraVariants();

}
