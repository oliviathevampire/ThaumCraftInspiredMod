package net.thegaminghuskymc.huskylib2.lib.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.thegaminghuskymc.huskylib2.lib.interf.IModBlock;
import net.thegaminghuskymc.huskylib2.lib.items.blocks.ItemModBlock;
import net.thegaminghuskymc.huskylib2.lib.utils.ProxyRegistry;

public abstract class BlockMod extends Block implements IModBlock {

    private final String[] variants;
    private String bareName;

    public BlockMod(String name, Material materialIn, String... variants) {
        super(materialIn);
        if (variants.length == 0) {
            variants = new String[]{name};
        }
        this.bareName = name;
        this.variants = variants;
        if (this.registerInConstruction()) {
            this.setUnlocalizedName(name);
        }

    }

    public Block setUnlocalizedName(String name) {
        super.setUnlocalizedName(name);
        this.setRegistryName(this.getPrefix(), name);
        ProxyRegistry.register(this);
        return this;
    }

    public ItemBlock createItemBlock(ResourceLocation name) {
        return new ItemModBlock(this, name);
    }

    public boolean registerInConstruction() {
        return true;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullBlock(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean canRenderInLayer(IBlockState state, BlockRenderLayer layer) {
        return layer == BlockRenderLayer.TRANSLUCENT || layer == BlockRenderLayer.CUTOUT || layer == BlockRenderLayer.CUTOUT_MIPPED || layer == BlockRenderLayer.SOLID;
    }

    public String[] getVariants() {
        return this.variants;
    }

    @Override
    public String getBareName() {
        return bareName;
    }

    @SideOnly(Side.CLIENT)
    public ItemMeshDefinition getCustomMeshDefinition() {
        return null;
    }

    public EnumRarity getBlockRarity(ItemStack stack) {
        return EnumRarity.COMMON;
    }

    public IProperty[] getIgnoredProperties() {
        return new IProperty[0];
    }

    public IProperty getVariantProp() {
        return null;
    }

    public Class getVariantEnum() {
        return null;
    }
}